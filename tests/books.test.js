const cds = require('@sap/cds')

describe('reading/writing hierarchies', () => {

    it('should prepare to sqlite in-memory', async () => {
        await cds.deploy(__dirname + '/../db').to('sqlite::memory:')
        expect(cds.entities).toBeDefined()
    })

    it('should read a book', async () => {
        const { Books } = cds.entities
        expect(await
            SELECT.onefrom(Books).where({ ID: 251 })
        ).toMatchObject(
            {
                ID: 251, title: 'The Raven', author_ID: 150, stock: 333
            }
        )
    })


})
